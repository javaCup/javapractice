
  package cz.ppfa.abde.controllers;
  
  import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cz.ppfa.abde.entities.Ctenar;
import cz.ppfa.abde.repositories.CtenarRepository;
import cz.ppfa.abde.services.StringService;
  
  @RestController
  @RequestMapping(path="/main") 
  public class MainController { 
	  
	  @Autowired
	  StringService stringService;
	  
	  @Autowired
	  CtenarRepository ctenarRepository;

	  @GetMapping("/convert") 
	  public String convert(@RequestParam String text) {
		  return stringService.makeConversion(text);
	  }
	  
	  @Transactional
	  @GetMapping("/ctenari") 
	  public List<Ctenar> getCtenar() {
		  return ctenarRepository.findAll();
	  }

  }
  
  
