package cz.ppfa.abde.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name="CTENAR")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Ctenar implements Serializable{

	@Id
	@GeneratedValue
	@Column(name="id")
	private int id;
	
	@Column(name="jmeno")
	private String jmeno;
	
	@Column(name="prijmeni")
	private String prijmeni;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getJmeno() {
		return jmeno;
	}

	public void setJmeno(String jmeno) {
		this.jmeno = jmeno;
	}

	public String getPrijmeni() {
		return prijmeni;
	}

	public void setPrijmeni(String prijmeni) {
		this.prijmeni = prijmeni;
	}
	

}
