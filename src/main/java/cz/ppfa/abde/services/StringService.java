package cz.ppfa.abde.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

@Service
public class StringService {
	
	public String makeConversion(String str) {
		
		List<Integer> pos = new ArrayList<>();
		List<Character> chars = str.chars().mapToObj(e->(char)e).collect(Collectors.toList());
		
		int i=0;
	    for (Character c: chars){
	    	if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u'){
	    		pos.add(i);}
 			i++;
	    }  
	    
	    Collections.reverse(chars);
	    
	    pos.stream().forEach(p->
    		chars.set(p, Character.toUpperCase(chars.get(p))));

		char preValue = 'a';
	    for (int b=0;b<chars.size();b++){
		    while(chars.get(b) == ' ' && preValue == ' '){
		    	chars.remove(b-1);
		    }
		    preValue = chars.get(b); 
	    } 
	    
	    String response = chars.stream()
	    		    .map(e->e.toString())
	    		    .reduce((acc, e) -> acc  + e)
	    		    .get();
	    
	    return response;
	}
}
