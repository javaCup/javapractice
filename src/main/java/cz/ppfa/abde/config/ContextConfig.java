package cz.ppfa.abde.config;


import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;


import org.springframework.boot.web.servlet.FilterRegistrationBean;

import org.springframework.context.annotation.Bean;


import org.springframework.context.annotation.Configuration;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import org.springframework.orm.jpa.JpaTransactionManager;

import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import org.springframework.transaction.support.AbstractPlatformTransactionManager;

import  org.springframework.web.filter.CharacterEncodingFilter;

@Configuration
@EnableJpaRepositories("cz.ppfa.abde.repositories")
public class ContextConfig {
    
	@Bean
	public AbstractPlatformTransactionManager transactionManager() {        
		return new JpaTransactionManager(entityManagerFactory());   
	}

	@Bean
	public EntityManagerFactory entityManagerFactory() {      
		final LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();       
		bean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());       
		bean.setPackagesToScan("cz.ppfa.abde.entities");       
		bean.setDataSource(dataSource());       
		final Properties props = new Properties();       
		props.setProperty("hibernate.hbm2ddl.auto", "update");       
		props.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL9Dialect");       
		//props.setProperty("hibernate.show_sql", "true");       
		//props.setProperty("hibernate.format_sql", "true");
		bean.setJpaProperties(props);
		bean.afterPropertiesSet();
		return bean.getObject();
	}

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		//dataSource.setUrl("jdbc:h2:file:~/abdex_db;DB_CLOSE_DELAY=-1;FILE_LOCK=NO");
		dataSource.setDriverClassName("org.postgresql.Driver");
		dataSource.setUrl( "jdbc:postgresql://localhost:5432/jpaTest");
		  dataSource.setUsername("postgres");
		  dataSource.setPassword("demon");
		return dataSource;   
	}

	@Bean
	public FilterRegistrationBean filterRegistrationBean() {
		CharacterEncodingFilter filter = new CharacterEncodingFilter();
		filter.setEncoding("UTF-8");
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(filter);
		registrationBean.addUrlPatterns("/*");
		return registrationBean;
	}

}