package cz.ppfa.abde.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ppfa.abde.entities.Ctenar;

@Transactional
public interface CtenarRepository extends JpaRepository<Ctenar, Integer>{

	/*
	 * @Query("select p from Post p where p.id = ?1") public List<Post>
	 * getPostById(long id);
	 */
	 	
}